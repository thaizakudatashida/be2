    @extends('userview/base')
    @section('userview/content')


        <main class="page shopping-cart-page">
            <section class="clean-block clean-cart dark">
                <div class="container">
                    <div class="block-heading"> </div>
                    <div class="content">
                        <div class="row g-0">
                            <div class="col-md-12 col-lg-8">
                                <div class="items">
                                    <div class="product">
                                        <div class="row justify-content-center align-items-center">
                                            <div class="col-md-3">
                                                <a style="text-decoration: none;" href="./product-page"><img class="img-fluid d-block mx-auto"
                                                        src="assets/img/tech/image2.jpg"></a>
                                            </div>
                                            <div class="col-md-5 product-info"><a style="text-decoration: none;" class="product-name"
                                                    href="./product-page">Lorem Ipsum
                                                    dolor</a>
                                                <div class="product-specs">
                                                    <div><span>Display:&nbsp;</span><span class="value">5
                                                            inch</span></div>
                                                    <div><span>RAM:&nbsp;</span><span class="value">4GB</span>
                                                    </div>
                                                    <div><span>Memory:&nbsp;</span><span class="value">32GB</span>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="col-6 col-md-2 quantity"><label class="form-label d-none d-md-block"
                                                    for="quantity">Quantity</label><input type="number" id="number"
                                                    class="form-control quantity-input" value="1" min="0"></div>
                                            <div class="col-6 col-md-2 price"><span>$120</span></div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="col-md-12 col-lg-4">
                                <div class="summary">
                                    <h3>Summary</h3>
                                    <h4><span class="text">Subtotal</span><span class="price">$360</span>
                                    </h4>
                                    <h4><span class="text">Discount</span><span class="price">$0</span>
                                    </h4>
                                    <h4><span class="text">Shipping</span><span class="price">$0</span>
                                    </h4>
                                    <h4><span class="text">Total</span><span class="price">$360</span>
                                    </h4>
                                    <a style="text-decoration: none;"  href="./payment-page"><button class="btn btn-primary btn-lg d-block w-100"
                                            type="button">Payment</button></a>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </section>
        </main>
    @stop
