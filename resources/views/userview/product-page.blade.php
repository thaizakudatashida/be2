@extends('userview/base')
@section('userview/content')


    <main class="page product-page">
        <section class="clean-block clean-product dark">
            <div class="container">
                <div class="block-heading"></div>
                <div class="block-content">
                    <div class="product-info">
                        <div class="row">
                            <div class="col-md-6">
                                <div class="gallery">
                                    <div id="product-preview" class="vanilla-zoom">
                                        <div class="zoomed-image"></div>
                                        <div class="sidebar"><img class="img-fluid d-block small-preview"
                                                src="assets/img/tech/image1.jpg"><img
                                                class="img-fluid d-block small-preview"
                                                src="assets/img/tech/image1.jpg"><img
                                                class="img-fluid d-block small-preview" src="assets/img/tech/image1.jpg">
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="col-md-6">
                                <div class="info">
                                    <h3>Lorem Ipsum</h3>
                                    <div class="price">
                                        <h3>$300.00</h3>
                                    </div>
                                    <a style="text-decoration: none;" href="./shopping-cart"><button class="btn btn-primary" type="button"><i
                                                class="icon-basket"></i>Add to Cart</button></a>
                                    <div class="summary">
                                        <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Donec augue nunc,
                                            pretium at augue at, convallis pellentesque ipsum. Vestibulum diam risus,
                                            sagittis at fringilla at, pulvinar vel risus. Vestibulum dignissim eu nulla
                                            eu imperdiet. Morbi mollis tellus a nunc vestibulum consequat. Quisque
                                            tristique elit et nibh dapibus sodales. Nam sollicitudin a urna sed iaculis.
                                        </p>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </section>
    </main>
@stop
