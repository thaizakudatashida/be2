@include('userview/header') 
@include('userview/nav')

@yield('userview/content')

@include('userview/footer')