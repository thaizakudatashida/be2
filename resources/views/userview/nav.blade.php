{{-- <body>
    <nav class="navbar navbar-light navbar-expand-lg fixed-top bg-white clean-navbar">
        <div class="container"><a style="text-decoration: none;" class="navbar-brand logo" href="./index">Brand</a><button data-bs-toggle="collapse"
                class="navbar-toggler" data-bs-target="#navcol-1"><span class="visually-hidden">Toggle
                    navigation</span><span class="navbar-toggler-icon"></span></button>
            <div class="collapse navbar-collapse" id="navcol-1">
                <ul class="navbar-nav ms-auto">
                    <li class="nav-item"><a style="text-decoration: none;" class="nav-link" href="index">Home</a></li>
                    <li class="nav-item"><a style="text-decoration: none;" class="nav-link" href="shopping-cart">Shopping Cart</a></li>
                    <li class="nav-item"><a style="text-decoration: none;" class="nav-link" href="login">Login</a></li>
                </ul>
            </div>
        </div>
    </nav> --}}
    <div class="app">
        <div class="header">
            <nav class="navbar navbar-expand-lg navbar-dark">
                <div class="container-fluid">
                    <a class="navbar-brand" href="#">Navbar</a>
                    <button class="navbar-toggler" type="button" data-bs-toggle="collapse"
                        data-bs-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false"
                        aria-label="Toggle navigation">
                        <span class="navbar-toggler-icon"></span>
                    </button>
                    <div class="collapse navbar-collapse" id="navbarSupportedContent">
                        <ul class="navbar-nav me-auto mb-2 mb-lg-0 nav_input ">
                        </ul>
                        {{-- <a href="search"> --}}
                            <form class="d-flex" role="search" action="search" method="GET">
                                <input class="form-control me-2 input_search " type="search" name="search" placeholder="Search" aria-label="Search">
                                <button class="btn_search " type="submit"><i class="fa fa-search"></i></button>
                              </form>
                        {{-- </a> --}}
                        <a href="login">
                            <i class="fa fa-user"></i>
                        </a>
                        <a href="index">
                            <i class="fa fa-shopping-bag"></i>
                        </a>
                        </form>
                    </div>
                </div>
            </nav>