@extends('userview/base')
@section('userview/content')


    <main class="page payment-page">
        <section class="clean-block payment-form dark">
            <div class="container">
                <div class="block-heading"></div>
                <form>
                    <div class="products">
                        <h3 class="title">Checkout</h3>
                        <div class="item"><span class="price">$200</span>
                            <p class="item-name">Product 1</p>
                            <p class="item-description">Lorem ipsum dolor sit amet</p>
                        </div>
                        <!-- <div class="item"><span class="price">$120</span>
                            <p class="item-name">Product 2</p>
                            <p class="item-description">Lorem ipsum dolor sit amet</p>
                        </div> -->
                        <div class="total"><span>Total</span><span class="price">$320</span></div>
                    </div>
                    <div class="card-details">
                        <div class="row">
                            <div class="mb-3 col-sm-12 ">
                                <a style="text-decoration: none;" href="./index"><button
                                        class="btn btn-primary d-block w-100" type="button"> Proceed</button></a>
                            </div>

                        </div>
                    </div>
                </form>
            </div>
        </section>
    </main>
@stop
