@extends('userview/base')
@section('userview/content')
    <main class="page landing-page">
        <section class="clean-block slider dark">
           @include('userview/index_slider')
        </section>
        <section class="clean-block clean-catalog dark">
            <div class="container">
                @include('userview/index_category')
        </section>
    </main>
@stop
