@extends('adminview/base')
@section('adminview/content')

    {{-- @foreach ($products as $product)
        <p>This is user {{ $product->id }}</p>
        <p>This is user {{ $product->name }}</p>
        <p>This is user {{ $product->price }}</p>
        <p>This is user {{ $product->description }}</p>
    @endforeach --}}
    <div class="container-fluid">
        <h3 class="text-dark mb-4">Team</h3>
        <div class="card shadow">
            <div class="card-body">
                <div class="table-responsive table mt-2" id="dataTable" role="grid" aria-describedby="dataTable_info">
                    <table class="table my-0" id="dataTable">
                        <thead>
                            <tr>
                                <th>ID</th>
                                <th>Product_Name</th>
                                <th>Product_Image</th>
                                <th>Product_Price</th>
                                <th>Product_Discription</th>
                                <th>Add</th>
                                <th>Fix</th>
                                <th>Delete</th>
                            </tr>
                        </thead>
                        <tbody>
                            @foreach ($products as $product)
                            <tr>
                                <td>{{ $product->id }}</td>
                                <td>{{ $product->name }}</td>
                                <td> <img class=" me-2" width="50" height="50"
                                        src="assets_admin/images/{{ $product->image }}"></td>
                                <td>{{ $product->price }}<br></td>
                                <td>{{ $product->description }}</td>
                            </tr>
                            @endforeach
                        </tbody>
                    </table>
                </div>
                <div class="row">
                    <div class="col-md-6">
                        <nav class="d-lg-flex justify-content-lg-end dataTables_paginate paging_simple_numbers">
                            <ul class="pagination">
                                <li class="page-item disabled">
                                    <a class="page-link" aria-label="Previous" href="#">
                                        <span aria-hidden="true">«</span>
                                    </a>
                                </li>
                                <li class="page-item active">
                                    <a class="page-link" href="#">1</a>
                                </li>
                                <li class="page-item">
                                    <a class="page-link" href="#">2</a>
                                </li>
                                <li class="page-item">
                                    <a class="page-link" href="#">3</a>
                                </li>
                                <li class="page-item">
                                    <a class="page-link" aria-label="Next" href="#">
                                        <span aria-hidden="true">»</span>
                                    </a>
                                </li>
                            </ul>
                        </nav>
                    </div>
                </div>
            </div>
        </div>
    </div>
    </div>
@stop
