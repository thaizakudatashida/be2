@include('adminview/header') 
@include('adminview/nav')

@yield('adminview/content')

@include('adminview/footer')