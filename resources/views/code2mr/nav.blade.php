<div class="app">
    <div class="header">
        <nav class="navbar navbar-expand-lg navbar-dark">
            <div class="container-fluid">
                <a class="navbar-brand" href="#">Navbar</a>
                <button class="navbar-toggler" type="button" data-bs-toggle="collapse"
                    data-bs-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false"
                    aria-label="Toggle navigation">
                    <span class="navbar-toggler-icon"></span>
                </button>
                <div class="collapse navbar-collapse" id="navbarSupportedContent">
                    <ul class="navbar-nav me-auto mb-2 mb-lg-0 nav_input ">
                    </ul>
                    {{-- <a href="search"> --}}
                        <form class="d-flex" role="search" action="search" method="GET">
                            <input class="form-control me-2 input_search " type="search" name="search" placeholder="Search" aria-label="Search">
                            <button class="btn_search " type="submit"><i class="fa fa-search"></i></button>
                          </form>
                    {{-- </a> --}}
                    <a href="login">
                        <i class="fa fa-user"></i>
                    </a>
                    <a href="index">
                        <i class="fa fa-shopping-bag"></i>
                    </a>
                    </form>
                </div>
            </div>
        </nav>