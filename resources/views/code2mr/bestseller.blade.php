<div class="bestseller">
    <h1>
        Best Sellers
    </h1>

    <!--==================== SWIPER ====================-->
    <div class="swiper new-swiper productcard " tabindex="-1">
        <div class="swiper-wrapper">
            {{-- <tbody>
                @foreach ($products as $product)
                <tr>
                    <td>{{ $product->id }}</td>
                    <td>{{ $product->name }}</td>
                    <td> <img class=" me-2" width="50" height="50"
                            src="assets_admin/images/{{ $product->image }}"></td>
                    <td>{{ $product->price }}<br></td>
                    <td>{{ $product->description }}</td>
                </tr>
                @endforeach
            </tbody> --}}
            @foreach ($products as $product)
            <div class="card swiper-slide">
                <a style="text-decoration: none;" href="{{route('product',["id" => $product->id])}}"><img
                    class="img-fluid d-block mx-auto"
                    src="assets_admin/images/{{ $product->image }}"></a>
                <h3 class="new__title">{{ $product->name }}</h3>
                <div class="new__prices">
                    <span class="new__price">{{ $product->price }}</span>
                </div>
            </div>
            @endforeach
            {{-- <div class="card swiper-slide">
                <img src="./public/img/tech/13_3.png" alt="">
                <h3 class="new__title">Iphone 10</h3>
                <div class="new__prices">
                    <span class="new__price">$11.99</span>
                </div>
            </div>
            <div class="card swiper-slide">
                <img src="./public/img/tech/ip1.png" alt="">
                <h3 class="new__title">Iphone 10</h3>
                <div class="new__prices">
                    <span class="new__price">$4.99</span>
                </div>
            </div>
            <div class="card swiper-slide">
                <img src="./public/img/tech/ip2.png" alt="">
                <h3 class="new__title">Rip</h3>
                <div class="new__prices">
                    <span class="new__price">Iphone 10</span>
                </div>
            </div>
            <div class="card swiper-slide">
                <img src="./public/img/tech/ip3.png" alt="">
                <h3 class="new__title">Iphone 10</h3>
                <div class="new__prices">
                    <span class="new__price">$5.99</span>
                </div>
            </div> --}}
        </div>
    </div>

    <h1>
        Best Sellers
    </h1>
    <!--==================== CATEGORY ====================-->
    <div class="category-part">
        <div class="category_products">
            <div class="product-card">
                <a href="#">Primary link → </a>
            </div>
            <div class="product-card">
                <a href="#">Primary link → </a>
            </div>
            <div class="product-card">
                <a href="#">Primary link → </a>
            </div>
        </div>
        <div class="category_products">
            <div class="product-card">
                <a href="#">Primary link → </a>
            </div>
            <div class="product-card">
                <a href="#">Primary link → </a>
            </div>
            <div class="product-card">
                <a href="#">Primary link → </a>
            </div>
        </div>
    </div>
</div>