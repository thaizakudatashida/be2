<!--==================== CART ====================-->
<main class="page product-page mt-5">
    <section class="clean-block clean-product dark">
        <div class="rating"><img src="/public/img/star.svg"><img src="/public/img/star.svg"><img
                src="/public/img/star.svg"><img src="/public/img/star-half-empty.svg"><img
                src="/public/img/star-empty.svg">
            Review(1)
        </div>
        <div class="container">
            <div class="block-content">
                <div class="product-info">
                    <div class="row">
                        <div class="col-md-6">
                            <div class="gallery">
                                <div id="product-preview" class="vanilla-zoom">
                                    <div class="zoomedimage">
                                        <img src="/public/images/{{ $product->image }}" alt="" srcset="" style="height: 400px; width: 400px;" >
                                    </div>
                                    <div class="sidebar">
                                        <img class="img-fluid d-block small-preview" src="/public/images/{{ $product->image }}">
                                        <img class="img-fluid d-block small-preview" src="/public/images/{{ $product->image }}">
                                        <img class="img-fluid d-block small-preview" src="/public/images/{{ $product->image }}">
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="col-md-6">
                            <div class="info">
                                @if(isset($product))
                                <h3>{{$product->name}}</h3>
                                <div class="price">
                                    <h3>{{$product->price}}</h3>
                                </div>
                                <p>{{$product->description}}
                                </p>
                                <div class="color_img" style="display: flex">
                                    <img href="#" class="color_btn"  src="/public/images/{{ $product->image }}" />
                                    <img href="#" class="color_btn" src="/public/images/{{ $product->image }}"/>
                                </div>
                                @endif
                                <div class="summary" style="display: flex">
                                    <input type="number" id="number" class=" number_card " value="1"
                                    min="0">
                                    <div class="btn abc " href="#">Primary link <i class="fa fa-shopping-bag"></i>
                                    </div>
                                    <div class="btn btn_card" disabled href="#">Primary link</div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
</main>
