<?php

namespace Database\Seeders;

use App\Models\Product;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Str;

class ProductSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public static function run()
    {
        $json = file_get_contents(storage_path('products.json'));
        $objs = json_decode($json, true);
        foreach ($objs as $obj) {
            Product::create([
                'name' => $obj['name'],
                'price' => $obj['price'],
                'image' => $obj['image'],
                'description' => $obj['description'],
            ]);
        }
    }
}
