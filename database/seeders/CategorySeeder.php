<?php

namespace Database\Seeders;

use App\Models\Category;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class CategorySeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public static function run()
    {
        $json = file_get_contents(storage_path('categories.json'));
        $objs = json_decode($json, true);
        foreach ($objs as $obj) {
            Category::create([
                'name' => $obj['name']
            ]);
        }
    }
}
