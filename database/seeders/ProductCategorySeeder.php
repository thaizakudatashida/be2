<?php

namespace Database\Seeders;

use App\Models\ProductCategory;
use Illuminate\Database\Seeder;

class ProductCategorySeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public static function run()
    {
        $json = file_get_contents(storage_path('products_categories.json'));
        $objs = json_decode($json, true);
        foreach ($objs as $obj) {
            ProductCategory::create([
                'category_id' => $obj['category_id'],
                'product_id' => $obj['product_id']
            ]);
        }
    }
}
