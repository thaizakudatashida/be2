<?php

namespace App\Http\Controllers;

use App\Models\Product;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\File;

class ProductController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        
        //
        $obj = new Product();
        $products = $obj->all();

        return view('adminview/product', ['products' => $products]);
    }
    public function productOnIndex()
    {
        //
        $obj = new Product();
        $products = $obj->all();

        return view('code2mr/bestseller', ['products' => $products]);
    }
    

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $bodyContent = $request->all();

        $price = $bodyContent['price'];
        $description = $bodyContent['description'];
        $categoryIdsList = $bodyContent['category'];
        if($request->hasFile('image')) {
            $image = time()."_".$request->file('image')->getClientOriginalName();
            $request->file('image')->move(public_path('public/images'), $image);
        }
        $name = $bodyContent['name'];

        Product::createProduct($name, $price, $image, $description, $categoryIdsList);
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\Product  $product
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $product = DB::table('products')->get()->where('id', $id)->first(); 
        //DB: get product by id
        return view('code2mr/productpage', ['product' => $product]);
        
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\Product  $product
     * @return \Illuminate\Http\Response
     */
    public function edit(Product $product)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\Product  $product
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request)
    {
        $bodyContent = $request->all();

        $price = $bodyContent['price'];
        $id = $bodyContent['id'];
        $updated_at = $bodyContent['updated_at'];
        $description = $bodyContent['description'];
        $categoryIdsList = $bodyContent['category'];
        if($request->hasFile('image')) {
            $image = time()."_".$request->file('image')->getClientOriginalName();
            $request->file('image')->move(public_path('public/images'), $image);
        }
        $name = $bodyContent['name'];

        Product::updateProduct($id, $name, $price, $image, $description, $categoryIdsList, $updated_at);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\Product  $product
     * @return \Illuminate\Http\Response
     */
    public function destroy(Product $product)
    {
        //
    }
}
