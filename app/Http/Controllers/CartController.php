<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Product;
use Illuminate\Support\Facades\DB;

class CartController extends Controller
{
    public function AddToCart(Request $request,$id)
    {
        $product = DB::table('products')->get()->where('id', $id)->first(); 
        if ($request->session()->has('cart')) {
            $cart =  session('cart');
            $index = array_search($product, array_column($cart,'product'));
            if(!$index){
                array_push($cart,['product'=>$product,'quantity'=>1]);
            }else{
                $cart[$index]['quantity']+=1;
            }
            session(['cart' => $cart]);
        }else{
            session(['cart' => [['product'=>$product,'quantity'=>1]]]);
        }
        //DB: get product by id
        return view('code2mr/productpage');
    }
}
