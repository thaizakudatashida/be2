<?php

namespace App\Http\Controllers;

use App\Models\User;
use DateTime;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Session;

class CustomAuthController extends Controller
{
    //
    public function index()
    {
        return view('userview/login');
    }
    public function registration()
    {
        return view('userview/registration');
    }
    public function customRegistration(Request $request)
    {
        $request->validate([
            'name' => 'required',
            'email' => 'required|email|unique:users',
            'password' => 'required|min:6',
        ]);

        $data = $request->all();
        $check = $this->create($data);

        return redirect("admin")->withSuccess('You have signed-in');
    }
    public function create(array $data)
    {
        $now = new DateTime();
        return User::create([
            'name' => $data['name'],
            'email' => $data['email'],
            'password' => Hash::make($data['password']),
            'create_at' => $now,
            'update_at' => $now,
        ]);
    }
    
    public function customLogin(Request $request)
    {
        $request->validate([
            'email' => 'required',
            'password' => 'required',
        ]);

        $credentials = $request->only('email', 'password');
        if (Auth::attempt($credentials)) {
            return redirect('admin')->withSuccess('Signed in');
        }
        $credentials['name'] = $credentials['email'];
        unset($credentials['email']);
        if (Auth::attempt($credentials)) {
            return redirect()->intended('admin')->withSuccess('Signed in');
        }

        var_dump(12);die;
        return redirect("login")->withSuccess('Login details are not valid');
    }
    public function Admin()
    {
        if (Auth::check()) {
            return view('adminview/admin');
        }

        return redirect("login")->withSuccess('You are not allowed to access');
    }
    public function signOut()
    {
        Session::flush();
        Auth::logout();

        return Redirect('login');
    }
}
