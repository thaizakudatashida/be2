<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class ProductCategory extends Model
{
    use HasFactory;
    protected $fillable = [
        'category_id',
        'product_id',
    ];

    static function deleteProductCategoryByProductId($id){
        return Self::where('product_id', $id)->delete();
    }
    static function createProductCategory($categoryId,$productId){
        return Self::create(['category_id' => $categoryId,'product_id'=>$productId]);
    }
}
