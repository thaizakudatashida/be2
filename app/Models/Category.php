<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Category extends Model
{
    use HasFactory;
    protected $fillable = [
        'name',
    ];

    static function getAllCategories()
    {
        return Self::all();
    }

    static function createCategory($name)
    {
        return Self::create(['name' => $name]);
    }

    static function upateCategory($id, $name, $updated_at)
    {
        return Self::where('id', $id)->where('updated_at', $updated_at)
            ->update([
                'name' => $name
            ]);
    }
    static function deleteCategory($id, $updated_at)
    {
        return Self::where('id', $id)->where('updated_at', $updated_at)
            ->delete();
    }
}
