<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Product extends Model
{
    use HasFactory;
    protected $fillable = [
        'name',
        'price',
        'image',
        'description',
    ];

    static function getProductsPerPages()
    {
        return Self::orderBy('id')->paginate(10);
    }

    static function getProductsPerPagesByCategory($categoryId)
    {
        return Self::join('product_categories', 'products.id', '=', 'product_categories.product_id')
            ->where('product_categories.category_id', $categoryId)->orderBy('id')->paginate(10);
    }

    static function getProductsPerPagesByName($search)
    {
        return Self::where('products.name', 'like', '%' . $search . '%')->orderBy('id')->paginate(10);
    }

    static function getProductNew()
    {
        return Self::orderBy('updated_at', 'DESC')->paginate(10);
    }

    static function createProduct($name, $price, $image, $description, $categoryIdsList)
    {
        $productId = Self::create([
            'name' => $name,
            'price' => $price,
            'image' => $image,
            'description' => $description,
        ])->id;
        foreach ($categoryIdsList as $categoryId) {
            ProductCategory::createProductCategory($categoryId, $productId);
        }
    }

    static function updateProduct($id, $name, $price, $image, $description, $categoryIdsList, $updated_at)
    {
        $product = Self::where('id', $id)->first();
        if (!empty($product)) {
            unlink(public_path('public/images/') . $image);
        }
        Self::where('id', $id)->where('updated_at', $updated_at)
            ->update([
                'name' => $name,
                'price' => $price,
                'image' => $image,
                'description' => $description,
            ]);
        ProductCategory::deleteProductCategoryByProductId($id);
        foreach ($categoryIdsList as $categoryId) {
            ProductCategory::createProductCategory($categoryId, $id);
        }
    }

    static function deleteProduct($id, $updated_at){
        $product = Self::where('id', $id)->where('updated_at', $updated_at)->first();
        if (!empty($product)) {
            unlink(public_path('public/images/') . $product->image);
            ProductCategory::deleteProductCategoryByProductId($product->id);
            Self::where('id', $id)->where('updated_at', $updated_at)->delete();
    }
    }
}
