<?php

use Illuminate\Support\Facades\Route;
use App\Http\Controllers\CustomAuthController;
use App\Http\Controllers\HomeController;
use App\Http\Controllers\ProductController;
use App\Http\Controllers\CategoryController;
/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

// Route::get('/', function () {
//     return view('userview/index');
// });

// Route::get('/', function () {
//     return view('code2mr/code2mr');
// });

// Route::get('/index', function () {
//     return view('userview/index');
// });
// Route::get('/index', function () {
//     [HomeController::class, 'index'];
// });
Route::get('/login', function () {
    return view('userview/login');
});

Route::get('/search', [HomeController::class, 'search']);

// phần link của giao diện cũ : ko sài 
Route::get('/payment-page', function () {
    return view('userview/payment-page');
});
Route::get('/index',[HomeController::class, 'index']);
Route::get('/',[HomeController::class, 'index']);
Route::get('productpage', function () {
    return view('code2mr/productpage');
});
Route::get('/product-page', function () {
    return view('userview/product-page');
});
Route::get('/registration', function () {
    return view('userview/registration');
});
Route::get('/shopping-cart', function () {
    return view('userview/shopping-cart');
});
// phần link của giao diện cũ : ko sài 


// phần link của admin 
Route::get('/admin', function () {
    return view('adminview/admin');
});
Route::get('/ad-product', function () {
    return view('adminview/product');
}); 

Route::get('cart/{id}', [CartController::class, 'AddToCart'])->name("add_to_cart");

Route::get('admin', [CustomAuthController::class, 'Admin']); 
Route::get('login', [CustomAuthController::class, 'index'])->name('login');
Route::post('custom-login', [CustomAuthController::class, 'customLogin'])->name('login.custom'); 
Route::get('registration', [CustomAuthController::class, 'registration'])->name('register-user');
Route::post('custom-registration', [CustomAuthController::class, 'customRegistration'])->name('register.custom'); 
Route::get('signout', [CustomAuthController::class, 'signOut'])->name('signout');

Route::get('product', [ProductController::class, 'index']);
Route::get('product/{id}', [ProductController::class, 'show'])->name("product");

